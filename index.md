
![Realistic](https://user-images.githubusercontent.com/81689727/113489134-01b47980-9488-11eb-920a-47e594b2003a.png)

Has it occurred to you while playing Cities: Skylines that the initial cash amount is a little low?
Are you interested in having a little more buying power, without the total sandbox feel of Unlimited Money?

The concept of this work-in-progress mod is access to an in-game credit line.
Full implementation is intended to be a line of credit limited to a configurable amount.
As with most real-world credit lines, interest will compound daily at 1/365th of a (configurable) APR.
Adding a cash advance to your total funds will be as easy as a couple of clicks, but...
Minimum payments will come due monthly, and failing to pay will have consequences.
Paying off the entire amount due or a custom amount at any time will also be options.

As of today (4/4/21), the source files for RealisticCashMoney Mod are available on a branch of a Github repository of the same name.
I have adopted the opensource code of GrantMeMoney by NJAldwin ('the weatherman' on Steam) as a skeleton for RCMM.
So far, I have merely altered file and variable names and such, and have redressed the button and other UI elements.
This starting-point project builds without error, warning, or exception, generating a .dll which works fine as a local mod.

There is much to do! I will be working on development as time allows.
If you are interested in using the code as-is for a local mod, have any ideas about the concept, or would even be interested in participating in its inception and realization, please visit the Github Repository, where there are many ways to make your presence known, including a little Discussion forum.

Thanks for reading. Hope to see you 'round the playground.
yr hmbl srvnt,
OldBlueBen


                                                                          ...appreciating github.io since 4/3/21...
